import numpy as np
import cv2 as cv
drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix,iy = -1,-1
# mouse callback function



def on_mouse(event,x,y,flags,param):
    global ix,iy,drawing,mode
    if event == cv.EVENT_LBUTTONDOWN:
        drawing = True
        print("drawing")
        
        ix,iy = x,y
    elif event == cv.EVENT_MOUSEMOVE:
        if drawing == True:
            if mode == True:
                cv.rectangle(param[0],(ix,iy),(x,y),(255,255,255),-1)
                print("rect")
            else:
                cv.circle(param[0],(x,y),5,(0,0,0),-1)
    elif event == cv.EVENT_RBUTTONDBLCLK:
        print(x,y)        
        
    elif event == cv.EVENT_LBUTTONUP:
        drawing = False
        if mode == True:
            cv.rectangle(param[0],(ix,iy),(x,y),(0,0,0),-1)
        else:
            cv.circle(param[0],(x,y),5,(0,0,0),-1)
            
