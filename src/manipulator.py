import numpy as np
import cv2
from cv2 import waitKey
#PLAYING AROUND WITH FILTER
cap = cv2.VideoCapture(0)    

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    #print(frame.size)= 921600 = 640*480*3 =RGB
    # Make Grey
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    ''' 
    print(np.shape(gray))
    for x in range(480):
        for y in range(640):
            #print ("this is x: {} and this is y: {}".format(x,y))
            if gray[x][y]<150:
                gray[x][y]=0
    '''
    #waitKey(0)
    #print(gray.size)= 307200 = 640*480*1
    #Make Canny
    edges = cv2.Canny(gray,60,200)
    #make laplacian
    la= cv2.Laplacian(gray,cv2.CV_32F)
    dst=gray.copy()
    dst.fill(0)
    alpha=0.5
    la=cv2.accumulateWeighted(dst, la, alpha)
    
    #Contours
    
    ret,thresh = cv2.threshold(gray,50,255,3)
    t2=thresh.copy()
    ret,thresh = cv2.threshold(thresh,40,0,0)
   
    image, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,
                                                  cv2.CHAIN_APPROX_SIMPLE)
    imc=image.copy()
    image.fill(0)
    imc.fill(0)
    for contour in contours:#only some
        if cv2.contourArea(contour) < 100:
            cv2.drawContours(image, contour, -1, (0, 0, 100), 1)
    frame2=frame.copy()
    #all
    allCont = cv2.drawContours(frame, contours, -1, (0, 100, 0), 1)
    
    
    
    
    cv2.imshow("thresh",t2)
    cv2.imshow("gray",thresh)
    cv2.imshow("edges",edges)
    cv2.imshow("laplacian",la)
    cv2.imshow("color frame",frame2)
    cv2.imshow('All Contours',allCont)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()