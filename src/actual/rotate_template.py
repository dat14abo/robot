import cv2
from numpy import shape
import numpy as np
from astropy.wcs.docstrings import copy
from cv2 import imshow



def rotate_img_no_crop(img, angle):
    # angle in degrees
    
    height, width = img.shape[:2]
    
    img_center = (height / 2, width / 2)
    
    rot_mat = cv2.getRotationMatrix2D(img_center, angle, 1)
    
    abs_cos = abs(rot_mat[0, 0])
    abs_sin = abs(rot_mat[0, 1])
    
    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)
    
    rot_mat[0, 2] += bound_w / 2 - img_center[0]
    rot_mat[1, 2] += bound_h / 2 - img_center[1]
    
    rot_img = cv2.warpAffine(img, rot_mat, (bound_w, bound_h), borderValue=(255, 255, 255))
    
    return rot_img

#dsnt work
def crop_outside_largest_contour(img):
    img3 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    f=img3.copy()
    canny = cv2.Canny(img, 50, 200)
    image, contours, hierarchy = cv2.findContours(canny, cv2.RETR_EXTERNAL,
                                                      cv2.CHAIN_APPROX_NONE)
    cv2.drawContours(image,contours,-1,(255),-1)
    
    for contour in contours:
        x1, y1, w, h = cv2.boundingRect(contour)      
        print("shape is: {}".format(cv2.boundingRect(contour)))
        cv2.rectangle(f, (x1, y1), (x1 + w, y1 + h) , (132))
        
        img=img[y1:y1+h,x1:x1+w]       
      #  else:
       #     x1, y1, w, h = cv2.boundingRect(contour)
                # cv2.rectangle(graycolor, (x1, y1), (x1 + w, y1 + h) , (255, 0, 0))
                # cv2.drawContours(dst, contour, -1, (0, 0, 0), -1)
    return img

