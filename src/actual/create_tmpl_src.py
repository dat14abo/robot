import cv2
from actual import rotate_template
import numpy as np
from actual import makesquare as ms


def vcon(comp,url):
    
    for x in range(15, 345, 30):
        print(x)
        col= rotate_template.rotate_img_no_crop(cv2.imread(url), x)
        col2=rotate_template.crop_outside_largest_contour(col)
        tmp=ms.square_to(55, col2, 255)
    
        comp=np.vstack((comp,tmp))
        #cv2.imshow('sdgsd',comp)
        #cv2.waitKey(0)
    return comp


url_0='data/template_typ_bricka.png'
composite_image_0=ms.square_to(55,cv2.imread(url_0),255)
fin=vcon(composite_image_0,url_0)
cv2.imshow('vcon1',fin)
cv2.waitKey(0)

url_1='data/template_kon.jpg'
comp_1=ms.square_to(55,cv2.imread(url_1),255)
vcon_1=vcon(comp_1,url_1)
fin=np.hstack((fin,vcon_1))

url_2='data/template_typ_rundskruv.png'              
comp_2=ms.square_to(55,cv2.imread(url_2),255)
vcon_2=vcon(comp_2,url_2)
fin=np.hstack((fin,vcon_2))


url_2='data/template_typ_rund_2.png'              
comp_4=ms.square_to(55,cv2.imread(url_2),255)
vcon_4=vcon(comp_4,url_2)
fin=np.hstack((fin,vcon_4))

cv2.imshow("con",fin)
cv2.waitKey(0)
cv2.destroyAllWindows()
cv2.imwrite("data/composite_template_extended.jpg",fin)
    
#cv2.imshow('ddd',composite_image)
#cv2.waitKey(0)