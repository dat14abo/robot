import cv2 as cv
import numpy as np
import glob
from actual import sc



def template_match(src,tmpl):
    #src=image to search, tmpl=template to search for.
    #src>tmpl, dim src= dim tmpl 

   
    w,h,_=np.shape(tmpl)
    loc=(int(w/2),int(h/2))#need offset for the smaller result image
    result=cv.matchTemplate(src,tmpl,cv.TM_CCOEFF_NORMED)
    #cv.imwrite("template_res.jpg",result)
    min, max, minloc, maxloc=cv.minMaxLoc(result)
    #returns min max corrolation and their locations (min,max,minloc,maxloc)
    #I need to become better with numpy
    t=(loc[0]+maxloc[0],loc[1]+maxloc[1])
    print("best find has corrolation of {} and is positioned at {}".format(max,t))
    type=int(t[0]/55)
    orient=int(t[1]/55)*30
    torient=(type,orient)
    print('which gives us type {} and orient {} degree'.format(type,orient))
    
    src_show=src.copy()
    cv.circle(src_show,t,30,255)
    cv.circle(result,maxloc,30,255)
    cv.imshow("res",result)
    cv.imshow("src",src_show)
    cv.imshow("obj",tmpl)
    cv.waitKey(0)
    cv.destroyAllWindows()
    
    return torient
    

def fill_square_white(img, bbox):
    cv.rectangle()

def match_folder(): 
    comp_temp=cv.imread('data/composite_template_extended.jpg')
    files_as_list=glob.glob("data/found/*.*")
    coords=[]
    tmp=[('','','','')]
    for file in files_as_list:
        img=cv.imread(file)
        r=file
        coords=r.rsplit(sep="-")
        #print('this is r: {}'.format(r))
        #print(coords)
        #print('Sending target to robot')
        torient=template_match(comp_temp,img)
        tmp.append((coords[1],coords[2],torient[0],torient[1]))
        print('Camera coords are: {}, {}'.format(coords[1],coords[2]))
        #print("formatted target tuple: ",tmp)
    print('this is target list', tmp)
    tmp=tmp[1:]
    print('this is target list', tmp)
    return tmp
#cv.imshow('adf',img)
#    cv.waitKey(0)
cv.destroyAllWindows()