import cv2
import numpy as np
import socket
from actual import sc, image_enhance, cut_up_image
from actual import TemplateMatch
from actual import parse_tuple_to_string



f=0
while (f!=113):#113='q'
    
    img=cv2.imread("data/template_white.PNG")
    image_and_cont_tuple=image_enhance.take_pic_and_enhance(200)
    cut_up_image.separate_found_objects(image_and_cont_tuple)
    list_of_targets=TemplateMatch.match_folder()
    print("list of targets ",list_of_targets)
    
    targets_as_strings=['']
    for x in list_of_targets:
        targets_as_strings.append(parse_tuple_to_string.parse(x))
    targets_as_strings.remove('')
    print(targets_as_strings)
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sc.init_socket(client_socket)
    sc.run_com_loop(targets_as_strings)
    
    cv2.imshow('MAIN',img)
    f=cv2.waitKey(0)
print("Demo fin")