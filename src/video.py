import cv2 as cv
import numpy as np
import os
import test
from cv2 import imshow
# from manipulator import gray
# import cannytrackbar as ct

alpha_slider_max = 200
beta_slider_max = 200
thresh_slider_max = 255
a = 80
b = 66
t = 0


def init_thresh():
    trackbar_name_t = 'thresh x %d' % thresh_slider_max
    cv.namedWindow("Thresh-frame")
    cv.createTrackbar(trackbar_name_t, "Thresh-frame", t, thresh_slider_max, on_trackbar_t)


def init_canny():
    trackbar_name_a = 'Alpha x %d' % alpha_slider_max
    trackbar_name_b = 'Beta x %d' % alpha_slider_max
    cv.namedWindow("Canny-frame")
    cv.createTrackbar(trackbar_name_a, "Canny-frame", 60, alpha_slider_max, on_trackbar_a)
    cv.createTrackbar(trackbar_name_b, "Canny-frame", 100, beta_slider_max, on_trackbar_b)


def on_trackbar_a(val):
    global a
    a = val    


def on_trackbar_b(val):
    global b
    b = val


def on_trackbar_t(val):
    global t
    t = val


print('...')
cap = cv.VideoCapture(0)
ret, rgb = cap.read()
img = cv.cvtColor(rgb, cv.COLOR_BGR2GRAY)
dst = rgb
ret, frame = cap.read()
init_canny()
# init_thresh()

print('Starting...')

while(True):
    
    if True:
    
        ret, frame = cap.read()
        # cv.imshow("frame", frame)
        img = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    #     cv.imshow("gray-frame", img)
        graycolor = cv.cvtColor(img, cv.COLOR_BAYER_GR2BGR)
        dst = cv.Canny(img, a, b)
        cv.imshow("Canny-frame", dst)
    #     ret,thresh = cv.threshold(img,t,255,0)
    #     am_thresh = cv.adaptiveThreshold(img,255,cv.ADAPTIVE_THRESH_MEAN_C,cv.THRESH_BINARY,11,2)
        ag_thresh = cv.adaptiveThreshold(dst, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 5, 2)
     #   cv.imshow("Thresh-frame",thresh)
      #  cv.imshow("Thresh-frame1",am_thresh)
        cv.imshow("Thresh-frame2", ag_thresh)
        image, contours, hierarchy = cv.findContours(ag_thresh, cv.RETR_TREE,
                                                      cv.CHAIN_APPROX_NONE)
        cv.imwrite("template_src.jpg", image)
        cv.namedWindow('cont')
        cv.setMouseCallback('cont', test.on_mouse, graycolor)
        
        print(len(contours))
        for contour in contours:  # only some
            if (cv.contourArea(contour) > 600) & (cv.contourArea(contour) < 1500):
                cv.drawContours(graycolor, contour, -1, (0, 255, 0), -1)
                # cv.fillPoly(graycolor, contour, (0, 0, 255))
                print(cv.boundingRect(contour))
                x1, y1, w, h = cv.boundingRect(contour)
    #             rad=np.sqrt(x2*x2+y2*y2)
    #             cv.circle(graycolor,(x1,y1),int(rad),(0,0,255))
    
                area = cv.contourArea(contour)
                
                text = "Area: {}".format(area)
                
                cv.putText(graycolor, text, (x1, y1), cv.FONT_HERSHEY_SIMPLEX, 1 / 2, (0, 0, 255))
                
                cv.rectangle(graycolor, (x1, y1), (x1 + w, y1 + h) , (255, 255, 255))
                
            else:
                x1, y1, w, h = cv.boundingRect(contour)
                # cv.rectangle(graycolor, (x1, y1), (x1 + w, y1 + h) , (255, 0, 0))
                cv.drawContours(dst, contour, -1, (0, 0, 0), -1)
               
        cv.imshow("cont", dst)
        cv.imshow("bounding", graycolor)
        
    if cv.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv.destroyAllWindows()  
    
