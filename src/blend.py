from __future__ import print_function
from __future__ import division
import cv2 as cv
import argparse
import numpy as np
alpha_slider_max = 100
title_window = 'Linear Blend'
def on_trackbar(val):
    alpha = val / alpha_slider_max
    beta = ( 1.0 - alpha )
    dst = cv.addWeighted(src1, alpha, src2, beta, 0.0)
    cv.imshow(title_window, dst)

src1 = cv.imread('template_src.jpg')
print('src has dimensions {}'.format(np.shape(src1)))
src2 = cv.imread('template_res.jpg')
#print('res has dimensions {}'.format(np.shape(src2)))
pad=np.pad(src2,((18,18),(20,20),(0,0)),'constant')
pad=np.pad(pad,((1,0),(0,0),(0,0)),'constant')
cv.imwrite("padded.jpg",pad)
src2 = cv.imread('padded.jpg')
print('res-padded has dimensions {}'.format(np.shape(pad)))
cv.namedWindow(title_window)
trackbar_name = 'Alpha x %d' % alpha_slider_max
cv.namedWindow('title_window')
cv.createTrackbar(trackbar_name, title_window , 0, alpha_slider_max, on_trackbar)
# Show some stuff
on_trackbar(0)
# Wait until user press some key
cv.waitKey()