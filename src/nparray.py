import numpy as np
'''
a=np.arange(9)
print(a)
b=np.reshape(a, (3,3))
print(b)
c=np.pad(b,2,'constant')
print(c)

a=np.arange(27)
b=np.reshape(a, (3,3,3))
print(b)
c=np.pad(b,((1,1),(1,1),(0,0)),'constant')

print('padded (3,3,3) is {} and its shape is {}'.format(c,np.shape(c)))
d=np.pad(c,(0,1),'constant')
print(d)
'''
a=np.zero